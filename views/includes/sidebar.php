<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href=" <?= BASEURL . "views/pages/"; ?>">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item <?= $sidebarSection == 'dashboard' ? 'active' : ''; ?>">
    <a class="nav-link" href=" <?= BASEURL . "views/pages/"; ?>">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Modules
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item <?= $sidebarSection == 'student' ? 'active' : ''; ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseStudent" aria-expanded="true" aria-controls="collapseStudent">
      <i class="fas fa-fw fa-cog"></i>
      <span>Student</span>
    </a>
    <div id="collapseStudent" class="collapse <?= $sidebarSection == 'student' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item <?= $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="<?= BASEPAGES; ?>manage-student.php">Manage Student</a>
        <a class="collapse-item <?= $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="<?= BASEPAGES; ?>add-student.php">Add Student</a>
      </div>
    </div>
  </li>



  <li class="nav-item <?= $sidebarSection == 'subject' ? 'active' : ''; ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSubject" aria-expanded="true" aria-controls="collapseSubject">
      <i class="fas fa-fw fa-cog"></i>
      <span>Subject</span>
    </a>
    <div id="collapseSubject" class="collapse <?= $sidebarSection == 'subject' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item <?= $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="<?= BASEPAGES; ?>manage-Subject.php">Manage Subject</a>
        <a class="collapse-item <?= $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="<?= BASEPAGES; ?>add-subject.php">Add Subject</a>
      </div>
    </div>
  </li>



  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->
