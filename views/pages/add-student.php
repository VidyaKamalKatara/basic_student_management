<?php
require_once __DIR__ . "/../../helper/init.php";
$pageTitle = "Basic Student Management | Add Student";
$sidebarSection = "student";
$sidebarSubSection = "add";
Util::createCSRFToken();
$errors = "";
if (Session::hasSession('errors')) {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}

$old = "";
if (Session::hasSession('old')) {
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <?php
    require_once __DIR__ . "/../includes/head-section.php";
    ?>

    <!-- PLACE TO ADD YOUR CUSTOM CSS -->
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php
        require_once __DIR__ . "/../includes/sidebar.php";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php
                require_once __DIR__ . "/../includes/navbar.php";
                ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Student</h1>
                        <a href="<?= BASEPAGES; ?>manage-student.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                            <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Student
                        </a>
                    </div>

                    <!-- Basic Card Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Add Student</h6>
                        </div>
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="<?= BASEURL; ?>helper/routing.php" method="POST" id="add-student">
                                            <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token'); ?>">

                                            <div class="row">
                                                <!-- FIRST NAME -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first_name">First Name</label>
                                                        <input type="text" name="first_name" class="form-control <?= $errors != '' && $errors->has("first_name") ? 'error' : '' ?>" placeholder="Enter First Name" value="<?= $old != '' && isset($old["first_name"]) ? $old["first_name"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != '' && $errors->has("first_name")) {
                                                            echo "<span class='error'>{$errors->first('first_name')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/FIRST NAME -->

                                                <!-- LAST NAME -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="last_name">Last Name</label>
                                                        <input type="text" name="last_name" class="form-control <?= $errors != '' && $errors->has("last_name") ? 'error' : '' ?>" placeholder="Enter Last Name" value="<?= $old != '' && isset($old["last_name"]) ? $old["last_name"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != '' && $errors->has("last_name")) {
                                                            echo "<span class='error'>{$errors->first('last_name')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/LAST NAME -->

                                                 <!-- EMAIL ID -->
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email_id">Email Id</label>
                                                        <input type="email" name="email_id" class="form-control <?= $errors != '' && $errors->has('email_id') ? 'error' : ''; ?>" placeholder="Enter Email ID" value="<?= $old != '' && isset($old['email_id']) ? $old['email_id'] : ''; ?>" />
                                                        <?php
                                                        if ($errors != '' && $errors->has('email_id')) {
                                                            echo "<span class='error'>{$errors->first('email_id')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/EMAIL ID -->

                                                 <!-- PHONE NO -->
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="phone_no">Phone No</label>
                                                        <input type="text" name="phone_no" class="form-control <?= $errors != '' && $errors->has('phone_no') ? 'error' : ''; ?>" placeholder="Enter Phone Number" value="<?= $old != '' && isset($old["phone_no"]) ? $old["phone_no"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != "" && $errors->has("phone_no")) {
                                                            echo "<span class='error'>{$errors->first('phone_no')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/PHONE NO -->

                                                <!-- GENDER -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="gender">Gender</label>
                                                        <select name="gender" class="form-control <?= $errors != '' && $errors->has('gender') ? 'error' : ''; ?>">
                                                            <option value="Male" <?= $old != '' && isset($old['gender']) == "Male" ? 'selected' : '' ?>>Male</option>
                                                            <option value="Female" <?= $old != '' && isset($old['gender']) == "Female" ? 'selected' : '' ?>>Female</option>
                                                        </select>
                                                        <?php
                                                        if ($errors != '' && $errors->has('gender')) {
                                                            echo "<span class='error'>{$errors->first('gender')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/GENDER -->

                                                 <!-- BLOOD GROUP -->
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="blood_group">blood_group</label>
                                                        <select name="blood_group" class="form-control <?= $errors != '' && $errors->has('blood_group') ? 'error' : ''; ?>">
                                                            <option value="A+" <?= $old != '' && isset($old['blood_group']) == "A+" ? 'selected' : '' ?>>A+</option>

                                                            <option value="A-" <?= $old != '' && isset($old['blood_group']) == "A-" ? 'selected' : '' ?>>A-</option>

                                                            <option value="B+" <?= $old != '' && isset($old['blood_group']) == "B+" ? 'selected' : '' ?>>B+</option>
                                                            
                                                            <option value="B-" <?= $old != '' && isset($old['blood_group']) == "B-" ? 'selected' : '' ?>>B-</option>

                                                            <option value="AB+" <?= $old != '' && isset($old['blood_group']) == "AB+" ? 'selected' : '' ?>>AB+</option>

                                                            <option value="AB-" <?= $old != '' && isset($old['blood_group']) == "AB-" ? 'selected' : '' ?>>AB-</option>

                                                            <option value="O+" <?= $old != '' && isset($old['blood_group']) == "O+" ? 'selected' : '' ?>>O+</option>
                                                            
                                                            <option value="O-" <?= $old != '' && isset($old['blood_group']) == "O-" ? 'selected' : '' ?>>O-</option>
                                                        </select>
                                                        <?php
                                                        if ($errors != '' && $errors->has('blood_group')) {
                                                            echo "<span class='error'>{$errors->first('blood_group')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/BLOOD GROUP -->


                                                <!-- AGE -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="age">AGE</label>
                                                        <input type="text" name="age" class="form-control <?= $errors != '' && $errors->has("age") ? 'error' : ''; ?>" placeholder="Enter Age" value="<?= $old != '' && isset($old["age"]) ? $old["age"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != "" && $errors->has("age")) {
                                                            echo "<span class='error'>{$errors->first('age')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/AGE -->
                                                <!-- ADDRESS DETAILS  -->
                                                <!-- Block NO -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="block_no">Block No</label>
                                                        <input type="text" name="block_no" class="form-control <?= $errors != '' && $errors->has("block_no") ? 'error' : ''; ?>" placeholder="Enter Block No" value="<?= $old != '' && isset($old["block_no"]) ? $old["block_no"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != "" && $errors->has("block_no")) {
                                                            echo "<span class='error'>{$errors->first('block_no')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/Block No -->

                                                     <!-- STREET -->
                                                     <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="street">STREET </label>
                                                        <input type="text" name="street" class="form-control <?= $errors != '' && $errors->has("street") ? 'error' : ''; ?>" placeholder="Enter Street " value="<?= $old != '' && isset($old["street"]) ? $old["street"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != "" && $errors->has("street")) {
                                                            echo "<span class='error'>{$errors->first('street')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/STREET -->

                                                     <!-- City  -->
                                                     <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="city">City </label>
                                                        <input type="text" name="city" class="form-control <?= $errors != '' && $errors->has("city") ? 'error' : ''; ?>" placeholder="Enter City" value="<?= $old != '' && isset($old["city"]) ? $old["city"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != "" && $errors->has("city")) {
                                                            echo "<span class='error'>{$errors->first('city')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/City -->

                                                     <!-- PINCODE -->
                                                     <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pincode">Pincode</label>
                                                        <input type="text" name="pincode" class="form-control <?= $errors != '' && $errors->has("pincode") ? 'error' : ''; ?>" placeholder="Enter Pincode" value="<?= $old != '' && isset($old["pincode"]) ? $old["pincode"] : ''; ?>" />
                                                        <?php
                                                        if ($errors != "" && $errors->has("pincode")) {
                                                            echo "<span class='error'>{$errors->first('pincode')}</span>";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/PINCODE -->
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary" name="add_student" value="addStudent"><i class="fa fa-check"></i> Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <!-- /.container-fluid -->

                        </div>
                    </div>
                    <!-- End of Main Content -->

                    <?php
                    require_once __DIR__ . "/../includes/footer.php";
                    ?>

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <?php
            require_once __DIR__ . "/../includes/scroll-to-top.php";
            ?>

            <?php
            require_once __DIR__ . "/../includes/core-scripts.php";
            ?>

            <!-- PAGE LEVEL SCRIPTS -->
            <?php
            require_once(__DIR__ . "/../includes/page-level/student/add-student-scripts.php");
            ?>

</body>

</html>