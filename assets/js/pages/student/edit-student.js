$(function(){
    $("#edit-student").validate({
        'rules': {
            'first_name': {
                required: true,
                minlength: 2,
                maxlength: 25
            },
            'last_name': {
                required: true,
                minlength: 2,
                maxlength: 25
            },
            
            'phone_no': {
                required: true,
                minlength: 10,
                maxlength:25
            },
            'email_id': {
                required: true,
                email: true,
                maxlength: 40
            },
            'gender': {
                required: true
            },
            'blood_group':{
                required:true
            },
            'age':{
                required:true
            }
            
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
});