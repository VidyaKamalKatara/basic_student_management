var TableDataTables = function(){
    var handleStudentTable = function(){
        var manageStudentTable = $("#manage-student-datatable");
        var baseURL=window.location.origin;
        var filePath="/helper/routing.php";
        var oTable = manageStudentTable.dataTable({
            "processing":true,
            "serverSide":true,
            "ajax":{
                url:baseURL+filePath,
                method:"POST",
                data:{
                    "page":"manage_student"
                }
            },
            "lengthMenu":[
                [5,10,15,30,-1],
                [5,10,15, 30 ,"All"]
            ],
            "order":[
                [1,"ASC"]
            ],
            "columnDefs":[{
                'orderable':false,
                'targets':[0,-1,-2]
            }],
        });
        // console.log(oTable);
        // console.log( $("#student_name").val(data[0].first_name));

       
        manageStudentTable.on('click','.delete',function(){
            id=$(this).attr('id');
            $('#record_id').val(id);
           
        });
    }
    return {
        init:function(){
            handleStudentTable();
        }
    }
}();
jQuery(document).ready(function(){
    TableDataTables.init();
})