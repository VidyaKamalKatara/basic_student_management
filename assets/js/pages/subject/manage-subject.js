var TableDataTables = function(){
    var handleSubjectTable = function(){
        var manageSubjectTable = $("#manage-subject-datatable");
        var baseURL=window.location.origin;
        var filePath="/helper/routing.php";
        var oTable = manageSubjectTable.dataTable({
            "processing":true,
            "serverSide":true,
            "ajax":{
                url:baseURL+filePath,
                method:"POST",
                data:{
                    "page":"manage_subject"
                }
            },
            "lengthMenu":[
                [5,10,20,-1],
                [5,10,20,"All"]
            ],
            "order":[
                [1,"ASC"]
            ],
            "columnDefs":[{
                'orderable':false,
                'targets':[0,-1]
            }],
        });

        manageSubjectTable.on('click','.edit',function(){
            id = $(this).attr('id');
            $('#subject_id').val(id);
            $.ajax({
                url:baseURL + filePath,
                method:"POST",
                data:{
                    "subject_id":id,
                    "fetch":"subject"
                },
                dataType:"json",
                success:function(data){
                    $("#subject_name").val(data[0].sub_name);
                    console.log( $("#subject_name").val(data[0].sub_name));
                }
            });
        });
       
        manageSubjectTable.on('click','.delete',function(){
            id=$(this).attr('id');
            $('#record_id').val(id);
            $.ajax({
                url: baseURL+ filePath,
                method: "POST",
                data:{
                    "subject_id":id,
                    "fetch":"subject"
                },
                dataType:"json",
                success:function(data){
                    $("#subject_name").val(data[0].name);
                }
            });
        });
    }

    return {
        init:function(){
            handleSubjectTable();
        }
    }
}();
jQuery(document).ready(function(){
    TableDataTables.init();
})