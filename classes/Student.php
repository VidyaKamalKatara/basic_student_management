<?php

require_once __DIR__."/../helper/requirements.php";

class Student{
    private $table = "student";
    private $table1 = "address";
    private $table2 = "student_address";

    private $database;
    protected $di;
    
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'first_name'=> [
                'required'=> true,
                'minlength'=> 2,
                'maxlength'=> 25
            ],
            'last_name'=> [
                'required'=> true,
                'minlength'=> 2,
                'maxlength'=> 25
            ],
           
            'phone_no'=> [
                'required'=> true,
                'minlength'=> 10,
                'maxlength'=> 25
            ],
            'email_id'=> [
                'required'=> true,
                'email'=> true,
                'maxlength'=> 40
            ],
            'gender'=> [
                'required'=> true
            ],
            'block_no'=> [
                'required'=> true
            ],
            'street'=> [
                'required'=> true
            ],
            'city'=> [
                'required'=> true
            ],
            'pincode'=> [
                'required'=> true
            ],
    
        ]);
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addStudent($data)
    {
        

        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();

                $student_data_to_be_inserted = ['first_name' => $data["first_name"], 'last_name' => $data["last_name"], 'email_id' => $data["email_id"], 'phone_no' => $data["phone_no"], 'gender'=>$data["gender"], 'blood_group'=>$data["blood_group"],  'age'=>$data["age"]] ;
                // die(var_dump($student_data_to_be_inserted));
                $student_id = $this->database->insert($this->table, $student_data_to_be_inserted);

                $student_address_data_to_be_inserted =  ['block_no'=>$data["block_no"], 'street'=>$data["street"], 'city'=>$data["city"], 'pincode'=>$data["pincode"]];
                // die(var_dump($student_address_data_to_be_inserted));
                
                $address_id = $this->database->insert($this->table1, $student_address_data_to_be_inserted);

               $Bridging_data_to_be_inserted = ['aid'=>$address_id, 's_id'=>$student_id];
            //    die(var_dump($Bridging_data_to_be_inserted));
                $student_address_data = $this->database->insert($this->table2, $Bridging_data_to_be_inserted);

               

                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                // die(var_dump($e));
                echo "in add_error";
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

  
    public function editStudent($data)
    {
        die(var_dump($data));
        $validation = $this->validateData($data);
        // Util::dd($data);
        if(!($validation->fails()))
        {
            //Validation was successful
            try
            {
                $this->database->beginTransaction();
                $student_data_to_be_edited = ['id'=>$data["id"],'first_name' => $data["first_name"], 'last_name' => $data["last_name"], 'email_id' => $data["email_id"], 'phone_no' => $data["phone_no"], 'gender'=>$data["gender"], 'blood_group'=>$data["blood_group"],'age'=>$data["age"]];
                
                $id = $student_data['id'];
                
                $this->database->update($this->table, $student_data, 'id = '.$id);

                 $student_address_data_to_be_edited =  ['id'=>$data["id"],'block_no'=>$data["block_no"], 'street'=>$data["street"], 'city'=>$data["city"], 'pincode'=>$data["pincode"]];
                 $id1 = $student_address_data['id'];
                 $this->database->update($this->table1, $student_address_data, 'id = '.$id1);
        
                $Bridging_data_to_be_edited = ['id'=>$data["id"],'aid'=>$address_id, 's_id'=>$student_id];
                $id2 = $Bridging_data_to_be_edited['id'];

                $student_address_data = $this->database->update($this->table2, $Bridging_data_to_be_edited, 'id = '.$id2);
                
               
                $this->database->commit();

                return EDIT_SUCCESS;
            }
            catch(Exception $e)
            {
                // die(var_dump($e));
                $this->database->rollback();
                return EDIT_ERROR;
            }
        }
        else{
            //Validation Failed
            return VALIDATION_ERROR;
        }
    }



public function getJSONDataForDataTable($draw, $searchParameter, $orderBy, $start,$length)
{
    $columns = ["sr_no", "first_name","last_name", "email_id", "phone_no", "gender", "blood_group", "age", "address"];

    //SELECT COUNT(student.id) as total_count FROM student JOIN student_address ON student.id=student_address.s_id JOIN address ON student_address.aid=address.id WHERE student.deleted=0
    $totalRowCountQuery = "SELECT COUNT(student.id) as total_count FROM student JOIN student_address ON student.id=student_address.s_id JOIN address ON student_address.aid=address.id WHERE student.deleted=0";

    $filteredRowCountQuery = "SELECT COUNT(student.id) as filtered_total_count FROM student JOIN student_address ON student.id=student_address.s_id JOIN address ON student_address.aid=address.id WHERE student.deleted=0";
    
    $query = "SELECT student.id, student.first_name ,student.last_name, student.email_id, student.phone_no, student.gender, student.blood_group, student.age, CONCAT ( address.block_no, address.street ,address.city,address.pincode) AS address FROM student JOIN student_address ON student.id=student_address.s_id AND student.deleted=0 JOIN address ON address.id = student_address.aid";

    if($searchParameter!= null)
    {
        // $query .=" AND name like '%{$searchParameter}%'";

        $query .= " AND student.first_name like '%{$searchParameter}%' OR student.last_name like '%{$searchParameter}%' OR student.email_id like '%{$searchParameter}%' OR student.phone_no like '%{$searchParameter}%' OR student.gender like '%{$searchParameter}%' OR student.blood_group like '%{$searchParameter}%' OR student.age like '%{$searchParameter}%' OR  CONCAT ( address.block_no, address.street,address.city,address.pincode) AS address like '%{$searchParameter}%'";


        $filteredRowCountQuery .= " AND student.first_name like '%{$searchParameter}%' OR student.last_name like '%{$searchParameter}%' OR student.email_id like '%{$searchParameter}%' OR student.phone_no like '%{$searchParameter}%' OR student.gender like '%{$searchParameter}%' OR student.blood_group like '%{$searchParameter}%' OR student.age like '%{$searchParameter}%' OR CONCAT ( address.block_no, address.street ,address.city,address.pincode) AS address like '%{$searchParameter}%'";
    }

    if($orderBy != null)
    {
        $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
    }

    else
    {
        $query .= " ORDER BY {$columns[0]} ASC";
    }
    if($length != -1)
    {
        $query .= " LIMIT {$start}, {$length}";
    }

    $totalRowCountResult = $this->database->raw($totalRowCountQuery);
    $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count:0;

    $filteredRowCountResult =$this->database->raw($filteredRowCountQuery);
    $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->filtered_total_count:0;

$filteredData = $this->database->raw($query);

$numberOfRowsToDisplay = is_array($filteredData) ?  count($filteredData) : 0;
$data = [];
for($i=0;$i<$numberOfRowsToDisplay;$i++)
{
    $basePages = BASEPAGES;
    $subarray = [];
    $subarray[] = $i+1;
    $subarray[] = $filteredData[$i]->first_name;
    $subarray[] = $filteredData[$i]->last_name;
    $subarray[] = $filteredData[$i]->email_id;
    $subarray[] = $filteredData[$i]->phone_no;
    $subarray[] = $filteredData[$i]->gender;
    $subarray[] = $filteredData[$i]->blood_group;
    $subarray[] = $filteredData[$i]->age;
    $subarray[] = $filteredData[$i]->address;
    $subarray[]= <<<BUTTONS
    <a href="{$basePages}edit-student.php?id={$filteredData[$i]->id}" class='edit btn btn-outline-primary m-1'><i class='fas fa-pencil-alt'></i></a>

    <button class ='delete btn btn-outline-danger' id='{$filteredData[$i]->id}' data-toggle="modal" data-target ="#deleteModal"><i class = 'fas fa-trash-alt'></i></button>

BUTTONS;

$data[] = $subarray;
}

$output= array(
    "draw"=>$draw,
    "recordsTotal"=>$numberOfFilteredRows,
    "recordsFiltered"=>$numberOfFilteredRows,
    "data"=>$data
);

echo json_encode($output);
die(var_dump($output));
}

public function getStudentInfoById($studentID, $mode=PDO::FETCH_OBJ)
{
    //$query = "SELECT * FROM {$this->table} WHERE deleted = 0 AND id = {$studentID}";
$query = "SELECT student.id, student.first_name ,student.last_name, student.email_id, student.phone_no, student.gender, student.blood_group, student.age, CONCAT ( address.block_no, address.street ,address.city,address.pincode) AS address FROM student JOIN student_address ON student.id=student_address.s_id AND student.deleted=0 JOIN address ON address.id = student_address.aid";
    $result = $this->database->raw($query, $mode);
    return $result;
    //  $query = "SELECT * FROM {$this->table1} WHERE deleted = 0 AND id = {$addressID}";
    // $result = $this->database->raw($query, $mode);
    // return $result;
}

 
public function delete($id)
{
    // try{
    //     $this->database->beginTransaction();
    //     $this->database->delete($this->table , "id={$id}");
    //     $this->database->commit();
    //     return DELETE_SUCCESS;
    // }catch(Exception $e){
    //     $this->database->rollback();
    //     return DELETE_ERROR;
    // }
}

}
