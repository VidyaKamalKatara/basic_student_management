<?php
//dir is a system call which points to the current directory n .. previous dir ko point krta h
class Config
{
    protected $config;
    public function __construct()
    {
        $this->config= parse_ini_file(__DIR__ ."/../../config.ini");
    }
    public function get(string $key)
    {
        if(isset($this->config[$key])){
            return $this->config[$key];
        }
        die("This config cannot be found : " .$key);
    }
}