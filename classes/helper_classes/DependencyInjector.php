<?php
// jaise auth m dependency inject krna parta h usske jagah yeh b wala class k pass 2 method h set aur get jo dependency inject krne k liyai h
class DependencyInjector
{
    private $dependencies = [];

    public function set(string $key,$value)
    {
        $this->dependencies[$key]=$value;
    }
    public function get(string $key)
    {
        if(isset($this->dependencies[$key])){
            return $this->dependencies[$key];
        }
        die('This dependency not found : ' .$key);
    }
}