<?php
require_once 'init.php';

if(isset($_POST['add_student']))
{
    //USER HAS REQUESTED TO ADD A NEW STUDENT
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('student')->addStudent($_POST);
        // die(var_dump($_POST));
        switch($result)
        {
            
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-student.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-student.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some prob in validating your data at server side!');
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-student.php');
                break;
        }
    }
}


if(isset($_POST['page']) && $_POST['page'] == 'manage_student')
{
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by= $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get("student")->getJSONDataForDataTable($draw,$search_parameter,$order_by, $start , $length);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'student')
{
    $student_id = $_POST['student_id'];
    $result = $di->get('student')->getStudentInfoById($student_id, PDO::FETCH_ASSOC);
    // die(var_dump($result));
    echo json_encode($result);
}

if(isset($_POST['edit_student']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('student')->editStudent($_POST );
        // die(var_dump($_POST));
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(EDIT_ERROR, 'There was problem while editing record, please try again later!');
                Util::redirect('manage-student.php');
                break;
            case EDIT_SUCCESS:
                Session::setSession(EDIT_SUCCESS, 'The record have been edited successfully!');
                // Util::dd();
                Util::redirect('manage-student.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some prob in validating your data at server side!');
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                $id = $_POST['id'];
                Util::redirect('edit-student.php?id='.$id);
                break;
        }
    }
}




if(isset($_POST['delete_student']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('student')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('manage-student.php');
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('manage-student.php');
                break;
           
        }
    }
}

/**************************************************SUBJECT************************************************/ 
if(isset($_POST['add_subject']))
{
    //USER HAS REQUESTED TO ADD A NEW SUBJECT
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('subject')->addSubject($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-subject.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-subject.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some prob in validating your data at server side!');
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-subject.php');
                break;
        }
    }
}

if(isset($_POST['page']) && $_POST['page'] == 'manage_subject')
{
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by= $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get("subject")->getJSONDataForDataTable($draw,$search_parameter,$order_by, $start , $length);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'subject')
{
    $subject_id = $_POST['subject_id'];
    $result = $di->get('subject')->getSubjectById($subject_id, PDO::FETCH_ASSOC);
    echo json_encode($result);
}

if(isset($_POST['edit_subject']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('subject')->update($_POST,$_POST['subject_id']);
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(EDIT_ERROR, 'There was problem while editing record, please try again later!');
                Util::redirect('manage-subject.php');
                break;
            case EDIT_SUCCESS:
                Session::setSession(EDIT_SUCCESS, 'The record have been edited successfully!');
                //  Util::dd($result);
                Util::redirect('manage-subject.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some prob in validating your data at server side!');
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Util::dd($di->get('validator')->errors());
                Session::setSession('old', $_POST);
                Util::redirect('manage-subject.php');
                break; 
        }
    }
}

if(isset($_POST['delete_subject']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('subject')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('manage-subject.php');
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('manage-subject.php');
                break;
           
        }
    }
}
